package ${packageName}${dotSubpackage}

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import ${applicationPackage}.R
import ${applicationPackage}.presentation.base.BaseFragment
import ${applicationPackage}.databinding.${underscoreToCamelCase(fragmentName)}Binding

class ${className} : BaseFragment() {

    private val viewModel by viewModel<${viewModelName}>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = ${underscoreToCamelCase(fragmentName)}Binding.inflate(inflater, container, false)
        binding.vm = viewModel
        return binding.root
    }

    companion object {
        fun newInstance(): ${className} {
			val fragment = ${className}()
			val args = Bundle()
			fragment.arguments = args
			return fragment
		}
    }
}
