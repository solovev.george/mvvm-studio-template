package ${packageName}${dotSubpackage}

import ${applicationPackage}.presentation.base.BaseViewModel
import javax.inject.Inject

class ${viewModelName} @Inject constructor(

)  : BaseViewModel() {
  
}
