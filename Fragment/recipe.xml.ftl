<?xml version="1.0"?>
<#import "root://activities/common/kotlin_macros.ftl" as kt>
<recipe>

    <instantiate from="res/layout/fragment_blank.xml.ftl"
                       to="${escapeXmlAttribute(resOut)}/layout/${escapeXmlAttribute(fragmentName)}.xml" />

    <open file="${escapeXmlAttribute(resOut)}/layout/${escapeXmlAttribute(fragmentName)}.xml" />

    <instantiate from="src/app_package/BlankFragment.kt.ftl"
                   to="${escapeXmlAttribute(srcOut)}/${subpackage}${className}.kt" />

    <open file="${escapeXmlAttribute(srcOut)}/${subpackage}${className}.kt" />

    <instantiate from="src/app_package/BlankViewModel.kt.ftl"
                   to="${escapeXmlAttribute(srcOut)}/${subpackage}${viewModelName}.kt" />

    <open file="${escapeXmlAttribute(srcOut)}/${subpackage}${viewModelName}.kt" />

</recipe>
